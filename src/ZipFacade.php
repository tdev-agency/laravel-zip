<?php

namespace TrushDev\Zip;

use Illuminate\Support\Facades\Facade;

/**
 * @method static Zip open(string $name)
 * @method static Zip create(string $name)
 * @method static Zip check(string $name)
 */
class ZipFacade extends Facade
{

    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'zip';
    }
}
